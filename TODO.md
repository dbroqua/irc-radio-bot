# IRC Radio Bot

## Les commandes

* ~~!artist <add/del> {nom de l'artiste}~~
* ~~!song <add/del> {nom du morceau}~~
* ~~!stats <day/week/lastweek/month/lastmonth/year/lastyear> <artist/song>~~
* ~~!when <day/week/lastweek/month/lastmonth/year/lastyear> <artist/song>~~
* ~~!help {artist/song/stats/when}~~
* !notifications <on/off/state>
* ~~!list <day/week/lastweek/month/lastmonth/year/lastyear> <artist>~~