
class Help {
  static say (say, where, message) {
    say(where, message, true, true)
  }

  /**
   * Fonction affichant l'aide générale
   * @param  {Function} say
   * @param  {String} where
   * @param  {Array} args
   * @param  {Array} allowedCmds
   */
  static show (say, where, args, allowedCmds) {
    if (args.length > 2) {
      switch (args[2]) {
        case '!artist':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!artist') !== -1) {
            Help.showArtistSong(say, where, args)
          }
          break
        case '!song':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!song') !== -1) {
            Help.showArtistSong(say, where, args)
          }
          break
        case '!stats':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!stats') !== -1) {
            Help.showStats(say, where)
          }
          break
        case '!when':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!when') !== -1) {
            Help.showWhen(say, where)
          }
          break
        case '!list':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!list') !== -1) {
            Help.showList(say, where)
          }
          break
        case '!notifications':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!notifications') !== -1) {
            Help.showNotifications(say, where)
          }
          break
        case '!stream':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!stream') !== -1) {
            Help.showStream(say, where)
          }
          break
        case '!mute':
          if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!mute') !== -1) {
            Help.showMute(say, where)
          }
          break
        default:
          break
      }
    } else {
      Help.say(say, where, 'IRC Radio Bot :: Help')
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!artist') !== -1) {
        Help.say(say, where, '!artist <add/del> {nom à sauvegarder si différent du morceau en cours}')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!song') !== -1) {
        Help.say(say, where, '!song <add/del> {nom à sauvegarder si différent du morceau en cours}')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!stats') !== -1) {
        Help.say(say, where, '!stats <period> {artist/song}')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!when') !== -1) {
        Help.say(say, where, '!when <period> <artist/song>')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!list') !== -1) {
        Help.say(say, where, '!list <period> <artist/song>')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!help') !== -1) {
        Help.say(say, where, '!help {command}')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!notifications') !== -1) {
        Help.say(say, where, '!notifications <on/off/state>')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!stream') !== -1) {
        Help.say(say, where, '!stream')
      }
      if (allowedCmds[0] === 'ALL' || allowedCmds.indexOf('!mute') !== -1) {
        Help.say(say, where, '!mute <on/off/state>')
      }
    }
    Help.say(say, where, '__ END __')
  }

  /**
   * Fonction affichant l'aide sur la commande !stream
   * @param  {Function} say
   * @param  {String} where
   * @param  {Array} args
   */
  static showStream (say, where, args) {
    Help.say(say, where, 'IRC Radio Bot :: Stream', false, true)
    Help.say(say, where, 'Affiche l\'url du flux radio pour ton player favoris', false, true)
    Help.say(say, where, '!stream')
  }

  /**
   * Fonction affichant l'aide sur la commande !mute
   * @param  {Function} say
   * @param  {String} where
   * @param  {Array} args
   */
  static showMute (say, where, args) {
    Help.say(say, where, 'IRC Radio Bot :: Mute')
    Help.say(say, where, 'Permet d\'activer ou de désactiver les messages publics du bot')
    Help.say(say, where, '!mute on')
    Help.say(say, where, '!mute off')
    Help.say(say, where, '!mute state')
  }

  /**
   * Fonction affichant l'aide sur les commandes !artist et !song
   * @param  {Function} say
   * @param  {String} where
   * @param  {Array} args
   */
  static showArtistSong (say, where, args) {
    Help.say(say, where, 'IRC Radio Bot :: Artist/Song')
    Help.say(say, where, `${args[2]} add {[facultatif] nom} < Permet de rajouter une notification sur ${args[2] === '!artist' ? 'cet artiste' : 'ce morceau'}`)
    Help.say(say, where, `${args[2]} del {[facultatif] nom} < Permet de supprimer une notification sur ${args[2] === '!artist' ? 'cet artiste' : 'ce morceau'}`)
  }

  /**
   * Fonction affichant l'aide sur la commande !stats
   * @param  {Function} say
   * @param  {String} where
   */
  static showStats (say, where) {
    Help.say(say, where, 'IRC Radio Bot :: Stats')
    Help.say(say, where, 'Permet d\'afficher le nombre de passage à l\'antenne d\'un artiste/morceau sur une période donnée')
    Help.say(say, where, '!stats day <title>')
    Help.say(say, where, '!stats week <title>')
    Help.say(say, where, '!stats month <title>')
    Help.say(say, where, '!stats year <title>')
    Help.say(say, where, '!stats lastday <title>')
    Help.say(say, where, '!stats yesterday <title>')
    Help.say(say, where, '!stats lastweek <title>')
    Help.say(say, where, '!stats lastmonth <title>')
    Help.say(say, where, '!stats lastyear <title>')
    Help.say(say, where, 'Si <title> n\'est pas spécifie la stats concerne le nombre d\'artistes différents pour cette période')
  }

  /**
   * Fonction affichant l'aide sur la commande !when
   * @param  {Function} say
   * @param  {String} where
   */
  static showWhen (say, where) {
    Help.say(say, where, 'IRC Radio Bot :: When')
    Help.say(say, where, 'Permet d\'afficher la dernier passage d\'un artiste/morceau sur une période donnée')
    Help.say(say, where, '!when day <title>')
    Help.say(say, where, '!when week <title>')
    Help.say(say, where, '!when month <title>')
    Help.say(say, where, '!when year <title>')
    Help.say(say, where, '!when lastday <title>')
    Help.say(say, where, '!when yesterday <title>')
    Help.say(say, where, '!when lastweek <title>')
    Help.say(say, where, '!when lastmonth <title>')
    Help.say(say, where, '!when lastyear <title>')
  }

  /**
   * Fonction affichant l'aide sur la commande !list
   * @param  {Function} say
   * @param  {String} where
   */
  static showList (say, where) {
    Help.say(say, where, 'IRC Radio Bot :: List')
    Help.say(say, where, 'Permet d\'afficher la liste des titres joués pour un artiste sr une période donnée')
    Help.say(say, where, '!list day <artist>')
    Help.say(say, where, '!list week <artist>')
    Help.say(say, where, '!list month <artist>')
    Help.say(say, where, '!list year <artist>')
    Help.say(say, where, '!list lastday <artist>')
    Help.say(say, where, '!list yesterday <artist>')
    Help.say(say, where, '!list lastweek <artist>')
    Help.say(say, where, '!list lastmonth <artist>')
    Help.say(say, where, '!list lastyear <artist>')
  }

  /**
   * Fonction affichant l'aide sur la commande !notifications
   * @param  {Function} say
   * @param  {String} where
   */
  static showNotifications (say, where) {
    Help.say(say, where, 'IRC Radio Bot :: Notifications')
    Help.say(say, where, 'Permet d\'activer et de désactiver les notifications pour soit')
    Help.say(say, where, '!notifications off')
    Help.say(say, where, '!notifications off')
    Help.say(say, where, '!notifications state < Permet de voir si l\'on a désactivé ou non les notifications')
  }
}

module.exports = Help
