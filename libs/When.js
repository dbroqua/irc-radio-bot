const Queries = require('./Queries')
const moment = require('moment')

class When extends Queries {
  constructor (models) {
    super()
    this.models = models
  }

  /**
   * Point d'entrée
   * @param  {Function} botSay
   * @param  {String} where
   * @param  {Array} line
   */
  action (botSay, where, line) {
    if (line.length < 4) {
      botSay(where, 'Tu as oublié la période et ou l\'artiste/titre !')
      return false
    }

    let isOk = true
    this.setBaseFilter(line, 3, true)

    const period = line[2]
    const filter = this.getFilter()

    switch (period) {
      case 'day':
      case 'week':
      case 'month':
      case 'year':
      case 'lastday':
      case 'yesterday':
      case 'lastweek':
      case 'lastmonth':
      case 'lastyear':
        this.setPeriod(period)
        filter.createdAt = this.period
        break
      default:
        isOk = false
    }

    if (!isOk) {
      botSay(where, 'Période invalide !')
      return false
    }

    this.models.Histories
      .findOne(filter)
      .sort({
        createdAt: 'desc'
      })
      .limit(1)
      .then(item => {
        if (!item) {
          botSay(where, `${this.getValue()} n'a pas était joué pour cette période sur ${process.env.RADIO_ALIAS}`)
          return true
        }

        if (item.artist.toLowerCase() === this.getValue().toLowerCase()) {
          botSay(where, `Pour cette période ${this.getValue()} a été entendu pour la dernière fois le ${moment(item.createdAt).format('DD/MM/YYYY à HH:mm')} avec ${item.title}`)
        } else {
          botSay(where, `Pour cette période ${this.getValue()} a été entendu pour la dernière fois le ${moment(item.createdAt).format('DD/MM/YYYY à HH:mm')}`)
        }
      })
      .catch(err => {
        console.log('ERR:', err)
        botSay(where, 'Impossible de te répondre pour le moment, j\'ai buggé...')
      })
  }
}

module.exports = When
