const formatString = require('../helpers/strings').formatString

class ArtistSong {
  constructor (models) {
    this.models = models
  }

  /**
   * Fonction sauvegardant en historique le morceau en cours de lecture
   * @param  {Object} value
   */
  saveCurrent (value) {
    // Find if previous song was the same (on bot reload)
    this.models.Histories
      .find({
        radio: value.radio
      })
      .sort({
        createdAt: 'desc'
      })
      .limit(1)
      .exec((err, last) => {
        if (err ||
          last.length === 0 ||
          (last[0] !== undefined &&
            last[0].artist !== value.artist &&
            last[0].title !== value.title
          )
        ) {
          console.log('Save song!', value)
          // Previous song was different => save song!
          const history = new this.models.Histories(value)
          history.save()
        }
      })
  }

  /**
   * Point d'entrée pour l'ajout suppression de favoris
   * @param  {String} type
   * @param  {Function} botSay
   * @param  {String} from
   * @param  {Object} currentSong
   * @param  {Array} line
   */
  action (type, botSay, from, currentSong, line) {
    let value = currentSong[type]

    if (line.length > 3) {
      value = ''
      for (let i = 3; i < line.length; i += 1) {
        value += ' ' + line[i]
      }
      value = value.replace(' ', '')
    }

    const item = {
      user: from,
      type: type,
      value: value
    }

    switch (line[2]) {
      case 'add':
        this.add(botSay, from, item)
        break
      case 'del':
        this.delete(botSay, from, item)
    }
  }

  /**
   * Fonction permettant de notifier les utilisateurs lorsqu'un bon song est joué
   * @param  {Function} botSay
   * @param  {String} where
   * @param  {Object} currentSong
   */
  notify (botSay, where, currentSong) {
    this.models.Notifications
      .find({
        $or: [
          {
            type: 'artist',
            value: formatString(currentSong.artist)
          },
          {
            type: 'title',
            value: formatString(currentSong.title)
          }
        ]
      })
      .then(list => {
        const artist = []
        const song = []

        // On get la liste des utilisateurs ne souhaitant pas être notifié pour cette radio
        this.models.Preferences
          .find({
            radio: process.env.RADIO_ALIAS,
            notification: false
          })
          .then(prefs => {
            for (let i = 0; i < list.length; i += 1) {
              let isMute = false
              for (let j = 0; j < prefs.length; j += 1) {
                if (prefs[j].user === list[i].user) {
                  isMute = true
                  break
                }
              }

              if (!isMute) {
                if (list[i].type === 'artist') {
                  artist.push(list[i].user)
                } else {
                  song.push(list[i].user)
                }
              }
            }

            if (artist.length > 0) {
              botSay(where, `Hey ${artist.toString().replace(/,/g, ', ')} ! Y'a ${currentSong.artist} ! Monte${artist.length > 1 ? 'z' : ''} le son !`)
            }
            if (song.length > 0) {
              botSay(where, `Hey ${song.toString().replace(/,/g, ', ')} ! Y'a ${song.title} ! Monte${song.length > 1 ? 'z' : ''} le son !`)
            }
          })
      })
      .catch(err => {
        console.log('ERR:', err)
      })
  }

  /**
   * Fonction permettant d'ajouter un favoris
   * @param  {Function} botSay
   * @param  {String} from
   * @param  {Object} item
   */
  add (botSay, from, item) {
    this.models.Notifications
      .findOne(item)
      .then(notification => {
        if (!notification) {
          const newItem = new this.models.Notifications(item)
          newItem.save((err, res) => {
            if (err) {
              console.log('ERR:', err)
            } else {
              botSay(from, `${item.value} correctement ajouté dans vos favoris`)
            }
          })
        } else {
          botSay(from, `${item.value} est déjà dans tes favoris, c'est moche de vieillir...`)
        }
      })
      .catch(err => {
        console.log('ERR:', err)
      })
  }

  /**
   * Fonction permettant de supprimer un favoris
   * @param  {Function} botSay
   * @param  {String} from
   * @param  {Object} item
   */
  delete (botSay, from, item) {
    this.models.Notifications
      .findOne(item)
      .then(notification => {
        if (notification) {
          notification.remove((err, res) => {
            if (err) {
              console.log('ERR:', err)
            } else {
              botSay(from, `${item.value} correctement retiré dans vos favoris`)
            }
          })
        } else {
          botSay(from, `${item.value} n'est pas dans tes favoris, c'est moche de vieillir...`)
        }
      })
      .catch(err => {
        console.log('ERR:', err)
      })
  }
}

module.exports = ArtistSong
