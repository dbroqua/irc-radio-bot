const Queries = require('./Queries')

class When extends Queries {
  constructor (models) {
    super()
    this.models = models
  }

  /**
   * Point d'entrée
   * @param  {Function} botSay
   * @param  {String} where
   * @param  {Array} line
   */
  action (botSay, where, line) {
    if (line.length < 4) {
      botSay(where, 'Tu as oublié la période et ou l\'artiste !')
      return false
    }

    let isOk = true
    this.setBaseFilter(line, 3, false)

    const period = line[2]
    const filter = this.getFilter()

    switch (period) {
      case 'day':
      case 'week':
      case 'month':
      case 'year':
      case 'lastday':
      case 'yesterday':
      case 'lastweek':
      case 'lastmonth':
      case 'lastyear':
        this.setPeriod(period)
        filter.createdAt = this.period
        break
      default:
        isOk = false
    }

    if (!isOk) {
      botSay(where, 'Période invalide !')
      return false
    }

    this.models.Histories
      .find(filter)
      .then(items => {
        if (items.length === 0) {
          botSay(where, `${this.getValue()} n'a pas était joué pour cette période sur ${process.env.RADIO_ALIAS}`)
          return true
        } else {
          const list = []

          for (let i = 0; i < items.length; i += 1) {
            if (list.indexOf(items[i].title) === -1) {
              list.push(items[i].title)
            }
          }

          // list.push('__ END __')
          let index = 0
          const max = list.length
          const _call = setInterval(() => {
            botSay(where, `${index + 1}/${max} - ${list[index]}`)
            index += 1
            if (index === list.length) {
              clearInterval(_call)
            }
          }, 750)
        }
      })
      .catch(err => {
        console.log('ERR:', err)
        botSay(where, 'Impossible de te répondre pour le moment, j\'ai buggé...')
      })
  }
}

module.exports = When
