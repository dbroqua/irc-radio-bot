const Queries = require('./Queries')

class When extends Queries {
  constructor (models) {
    super()
    this.models = models
  }

  /**
   * Point d'entrée
   * @param  {Function} botSay
   * @param  {String} where
   * @param  {Array} line
   */
  action (botSay, where, line) {
    let isOk = true
    if (line.length < 3) {
      botSay(where, 'Tu as oublié la période !')
      return false
    }

    this.setBaseFilter(line, 3, true)

    const period = line[2]
    const filter = this.getFilter()

    switch (period) {
      case 'day':
      case 'week':
      case 'month':
      case 'year':
      case 'lastday':
      case 'yesterday':
      case 'lastweek':
      case 'lastmonth':
      case 'lastyear':
        this.setPeriod(period)
        filter.createdAt = this.period
        break
      default:
        isOk = false
    }

    if (!isOk) {
      botSay(where, 'Période invalide !')
      return false
    }

    if (line.length === 3) {
      this.getCountGroups(botSay, where)
    } else {
      this.models.Histories
        .find(filter)
        .sort({
          createdAt: 'desc'
        })
        .then(items => {
          if (items.length === '0') {
            botSay(where, `${this.getValue()} n'a pas était joué pour cette période sur ${process.env.RADIO_ALIAS}`)
            return true
          }

          botSay(where, `Pour cette période ${this.getValue()} a été entendu ${items.length} fois`)
        })
        .catch(err => {
          console.error('ERR:', err)
          botSay(where, 'Impossible de te répondre pour le moment, j\'ai buggé...')
        })
    }
  }

  getCountGroups (botSay, where) {
    const artists = []
    const songs = []
    this.models.Histories
      .find({
        radio: process.env.RADIO_ALIAS,
        createdAt: this.filter.createdAt
      })
      .sort({
        artist: 'desc'
      })
      .then(items => {
        if (items.length === '0') {
          botSay(where, 'Je n\'ai absolu rien entendu pendant cette période !')
          return true
        }

        for (let i = 0; i < items.length; i += 1) {
          if (artists.indexOf(items[i].artist) === -1) {
            artists.push(items[i].artist)
          }
          if (songs.indexOf(items[i].title) === -1) {
            songs.push(items[i].title)
          }
        }

        botSay(where, `Pour cette période tu as pu entendre ${artists.length} artistes différents pour un total de ${songs.length} morceaux différents !`)
      })
      .catch(err => {
        console.error('ERR:', err)
        botSay(where, 'Impossible de te répondre pour le moment, j\'ai buggé...')
      })
  }
}

module.exports = When
