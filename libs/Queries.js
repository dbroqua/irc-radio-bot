const moment = require('moment')
const formatString = require('../helpers/strings').formatString

class Queries {
  constructor () {
    this.filter = null
    this.value = null
    this.period = null
  }

  /**
   * Fonction permettant de générer la query sur artist et title
   * @param  {Array} line
   * @param  {Integer} start
   * @param {Boolean} artistAndTitle
   */
  setBaseFilter (line, start, artistAndTitle) {
    let value = ''
    for (let i = start; i < line.length; i += 1) {
      value += ' ' + line[i]
    }
    this.value = value.replace(' ', '')
    value = formatString(this.value)

    if (artistAndTitle) {
      this.filter = {
        $or: [
          {
            artist: value
          },
          {
            title: value
          }
        ]
      }
    } else {
      this.filter = {
        artist: value
      }
    }
  }

  /**
 * Fonction permettant de générer le filtre de date sur les commandes !stats et !when
 * @param  {String} period
 */
  setPeriod (period) {
    switch (period) {
      case 'day':
        this.period = {
          $gte: moment().startOf('day'),
          $lte: moment().endOf('day')
        }
        break
      case 'week':
        this.period = {
          $gte: moment().startOf('week'),
          $lte: moment().endOf('day')
        }
        break
      case 'month':
        this.period = {
          $gte: moment().startOf('month'),
          $lte: moment().endOf('day')
        }
        break
      case 'year':
        this.period = {
          $gte: moment().startOf('year'),
          $lte: moment().endOf('day')
        }
        break
      case 'lastday':
      case 'yesterday':
        this.period = {
          $gte: moment().subtract(1, 'day').startOf('day'),
          $lte: moment().subtract(1, 'day').endOf('day')
        }
        break
      case 'lastweek':
        this.period = {
          $gte: moment().subtract(1, 'week').startOf('week'),
          $lte: moment().subtract(1, 'week').endOf('week')
        }
        break
      case 'lastmonth':
        this.period = {
          $gte: moment().subtract(1, 'month').startOf('month'),
          $lte: moment().subtract(1, 'month').endOf('month')
        }
        break
      case 'lastyear':
        this.period = {
          $gte: moment().subtract(1, 'year').startOf('year'),
          $lte: moment().subtract(1, 'year').endOf('year')
        }
        break
      default:
        this.period = {}
    }
  }

  /**
   * Getter pour le filter
   * @return {Object}
   */
  getFilter () {
    return this.filter
  }

  /**
   * Getter pour le texte filtré
   * @return {String}
   */
  getValue () {
    return this.value
  }

  /**
   * Getter pour la période
   * @return {String}
   */
  getPeriod () {
    return this.period
  }
}

module.exports = Queries
