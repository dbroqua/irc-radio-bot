class Preferences {
  constructor (models) {
    this.models = models
  }

  notifications (botSay, where, from, exploded) {
    if (exploded.length < 3) {
      botSay(where, 'Attention ! tu as oublié de dire ce que tu voulais faire !')
      return false
    }

    const notification = (exploded[2] === 'on')
    const radio = process.env.RADIO_ALIAS

    const errorCatcher = (err) => {
      botSay(from, 'Oups... j\'ai pas réussi à faire ce que tu voulais :(')
      console.log('ERR:', err)
    }
    const successCatcher = () => {
      if (notification) {
        botSay(from, 'C\'est parti pour de la notification à gogo ! Yeah !')
      } else {
        botSay(from, 'Tu ne m\'aime plus ? c\'est ça ? :\'(')
      }
    }

    switch (exploded[2]) {
      case 'on':
      case 'off':
        this.models.Preferences
          .findOne({
            user: from,
            radio: radio
          })
          .then((item) => {
            if (!item) {
              const newItem = new this.models.Preferences({
                user: from,
                radio: radio,
                notification: notification
              })
              newItem.save()
                .then(successCatcher)
                .catch(errorCatcher)
            } else {
              item.update({
                notification: notification
              })
                .then(successCatcher)
                .catch(errorCatcher)
            }
          })
          .catch(errorCatcher)

        break
      case 'state':
        this.models.Preferences
          .findOne({
            user: from,
            radio: radio
          })
          .then((item) => {
            if (!item) {
              botSay(from, 'Bein... vu que t\'as fait ni de on, ni de off je suis.... On ! 8)')
            } else {
              if (item.notification) {
                botSay(from, 'Toi et moi c\'est pour la vie ! tu n\'en louperas pas une miette !')
              } else {
                botSay(from, 'Je boude ! Je ne te dirai rien !')
              }
            }
          })
          .catch(errorCatcher)
        break
      default:
        botSay(where, `T'es fort toi! t'as le choix entre on/off/state et tu m'envoies ${exploded[2]}...`)
    }
  }
}

module.exports = Preferences
