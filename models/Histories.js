module.exports = mongoose => {
  const schema = new mongoose.Schema({
    radio: String,
    artist: String,
    title: String,
    createdAt: {
      type: Date,
      default: Date.now
    }
  })

  const Histories = mongoose.model('Histories', schema)

  return Histories
}
