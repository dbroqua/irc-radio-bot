module.exports = mongoose => {
  const schema = new mongoose.Schema({
    user: String,
    notification: Boolean,
    radio: String
  })

  const Preferences = mongoose.model('Preferences', schema)

  return Preferences
}
