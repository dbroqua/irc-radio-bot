module.exports = mongoose => {
  const schema = new mongoose.Schema({
    user: String,
    type: {
      type: String,
      enum: ['artist', 'title']
    },
    value: String
  })

  const Notifications = mongoose.model('Notifications', schema)

  return Notifications
}
