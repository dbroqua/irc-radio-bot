const fs = require('fs')
const path = require('path')
const mongoose = require('mongoose')

const basename = path.basename(__filename)

mongoose.set('useNewUrlParser', true)
mongoose.set('useUnifiedTopology', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
mongoose.set('debug', process.env.DEBUG || false)

mongoose.connect(process.env.MONGO_URL)

const db = mongoose.connection

const getSchemas = () => {
  const m = {}

  fs.readdirSync(__dirname)
    .filter(file => {
      return (
        file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
      )
    })
    .forEach(file => {
      const model = require(path.resolve(__dirname, file))(mongoose)
      m[model.modelName] = model
    })

  return m
}

db.on('error', console.error.bind(console, 'Mongodb connection error:'))

const models = getSchemas()

module.exports = {
  models: models,
  mongoose: mongoose
}
