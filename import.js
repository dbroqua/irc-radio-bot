const path = require('path')
const moment = require('moment')
const models = require('./models').models

// Export data from base:
// docker exec mongo sh -c 'mongoexport --host localhost --db irc-bot-rx3 --collection histories --type=json --fields artist,songName,createdAt' > rx3.json

if (!process.argv[2]) {
  console.error(new Error('Missing radio alias'))
  process.exit(1)
}

const radio = process.argv[2]
const file = path.join('import', `${radio}.json`)

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream(file)
})

let lines = 0
let checked = 0
let inserted = 0
let canDone = false

const done = () => {
  if (canDone && lines === checked) {
    console.log(`Inserted ${inserted} on ${lines} lines read`)
    process.exit(0)
  }
}

lineReader.on('close', () => {
  console.log('All lines read, waiting for insert')
  canDone = true
  done()
})

lineReader.on('line', function (line) {
  lines += 1
  const currentLine = JSON.parse(line)

  const value = {
    radio: radio,
    artist: currentLine.artist,
    title: currentLine.songName,
    createdAt: currentLine.createdAt.$date
  }

  // console.log('WHERE:', value)

  models.Histories
    .find({
      radio: radio,
      artist: currentLine.artist,
      title: currentLine.songName
    })
    .exec(function (err, saved) {
      // console.log('SAVED:', saved)
      if (err) {
        checked += 1
        console.error('ERR:', err)
        done()
      } else {
        let found = false
        // console.log('CHECK FOR:', value.title)
        for (let i = 0; i < saved.length; i += 1) {
          const currentDate = moment(saved[i].createdAt).millisecond(0)

          // console.log(currentDate.toDate(), moment(value.createdAt).millisecond(0).toDate())

          if (currentDate.diff(moment(value.createdAt).millisecond(0), 'seconds') === 0) {
            // console.log('======= FOUND =========')
            found = true
            break
          }
        }

        if (!found) {
          // Insert line
          const history = new models.Histories(value)
          history.save(() => {
            checked += 1
            inserted += 1
            done()
          })
        } else {
          checked += 1
          done()
        }
      }
    })
})
