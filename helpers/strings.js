/**
 * Fonction permettant d'être insensible à la casse
 * @param  {String} string
 */
function formatString (string) {
  if (string !== undefined && string !== null) {
    return new RegExp('^' + string.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&') + '$', 'i')
  }
  return ';'
}

module.exports = {
  formatString: formatString
}
