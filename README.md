# IRC Radio Bot

**Ré écriture complète**

IRC Radio Bot est un bot permettant de publier sur un canal IRC le morceau en cours de diffusion sur une radio de votre choix.

## Prérequis

* NodeJS
* MongoDB
* Yarn (ou npm)

## Configuration

Pour fonctionner ce bot a besoin d'un ensemble de variable d'environnement, en voici là liste :

```bash
# L'url du flux radion
export STREAM_URL="";
# L'alias de la radio (pour la sauvegarde en base)
export RADIO_ALIAS="ma-radio";
# L'adresse du serveur IRC
export IRC_SERVER="irc.freenode.net";
# Le pseudo du bot
export IRC_NICKNAME="IrcRadioBot";
# Le canal sur lequel le bot apparait
export IRC_CHANNEL="#foo";
# Le nom du bot
export IRC_USER_NAME="nodebot";
# Le nom réel du bot
export IRC_REAL_NAME="IRC RADIO BOT";
# L'adresse du serveur mongo et de la base associée
export MONGO_URL="mongodb://localhost/irc-radio-bot";
# Le bot souhaite la bienvenue aux arrivants ?
export SAY_HELLO="false"
```

Ne pas oublier d'installer les modules nodejs :

```bash
yarn install
```

## Lancement

```bash
yarn start
```

## Crédits

Développé par [DarKou](https://mamot.fr/@DarKou).

## Licence

IRC Radio Bot est distribué sous [licence MIT](LICENCE.txt)